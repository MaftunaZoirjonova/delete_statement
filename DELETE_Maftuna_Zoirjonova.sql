-- Task 1. Remove a previously inserted film from the inventory and all corresponding rental records

DELETE FROM rental
    WHERE inventory_id in (
        SELECT inventory_id
        FROM inventory
        WHERE film_id IN (
            SELECT film_id
            FROM film
            WHERE UPPER(title) = UPPER('Red Notice')
            )
        );

DELETE FROM inventory
WHERE film_id IN (
    SELECT film_id
    FROM film
    WHERE UPPER(title) = UPPER('Red Notice')
    );

DELETE FROM film_actor
WHERE film_id = (
    SELECT film_id
    FROM film
    WHERE UPPER(film.title) = UPPER('Red Notice')
    );

DELETE FROM film
WHERE UPPER(film.title) = UPPER('Red Notice');


DELETE FROM actor a
WHERE a.first_name = 'Gal' AND a.last_name = 'Gadot'
   OR a.first_name = 'Ryan' AND a.last_name = 'Reynolds'
   OR a.first_name = 'Dwayne' AND a.last_name = 'Johnson'
   OR a.first_name = 'Ritu' AND a.last_name = 'Arya';

-- Task 2. Remove any records related to you (as a customer) from all tables except "Customer" and "Inventory"

-- My customer ID that I have changed is customer_id=1, I updated this customer's data to mine in the Update section so 
-- as T understand I should delete data related to this customer except Customer and Inventory table so I think payments and rental data should be deleted
-- if my guesses are wrong just consider the above code and skip the below code. Since I have not created these payment and rental data but they belong to the customer I have changed
-- so I'm a bit confused whether it's right or wrong so I added just in case if it was necessary

DELETE FROM payment
WHERE customer_id = (SELECT customer_id
                    FROM customer
                    WHERE first_name = 'Maftuna' AND last_name = 'Zoirjonova'
                );

DELETE FROM rental
WHERE customer_id = (SELECT customer_id
                     FROM customer
                     WHERE first_name = 'Maftuna' AND last_name = 'Zoirjonova'
                );